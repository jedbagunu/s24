console.log('Hello world')

const number = 2**3
console.log(` The cube of 2 is: ${number}`)

const address = ['Street 1', 'Caloocan City', 'NCR']
const [street, city, province] = address
console.log(`I live in ${street} ${city} ${province}`)

const pet = {
	name: "Cooper",
	weight: 24, 
	age: 2,
	favorite_food: "chimken",
	hooman: "Jed"
}

const { name, weight, age, favorite_food, hooman } = pet
console.log(`Hello my name is ${name} I am ${weight} kgs, I am ${age} yrs old,
my favorite food is ${favorite_food} my Hooman is ${hooman}`)


 const numbers_loop = [1,2,3,4,5]
 numbers_loop.forEach((numbers_loop) => {
 	console.log(`${numbers_loop}`)
 })

const reduceNumber = [10,15,20,30]
reduceNumber.reduce((reduceNumber) => {
	console.log(`${reduceNumber}`)
})

const dog = {
	name: 'Cooper',
	age: '2',
	breed: 'mix'
}

console.log(dog)

